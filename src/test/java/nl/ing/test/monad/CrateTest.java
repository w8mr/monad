package nl.ing.test.monad;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import static nl.ing.test.monad.CrateTest.Color.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.core.IsNull.notNullValue;

public class CrateTest {

    private Crate<Bottle> crate;
    private List<Bottle> bottles;

    @Before
    public void setup() {

        bottles = Arrays.asList(
                Bottle.newInstance(GREEN, 33),
                Bottle.newInstance(GREEN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(WHITE, 75));
        crate = Crate.of(bottles);
    }

    @Test
    public void testOf() {
        assertThat(crate, is(notNullValue()));
        assertThat(crate.getContent(), hasItems(
                Bottle.newInstance(GREEN, 33),
                Bottle.newInstance(GREEN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(WHITE, 75)));
    }

    @Test
    public void testMap() {
        Crate crateAfterMap = crate.map(b -> b.drink(10));
        List<Bottle> content = crateAfterMap.getContent();
        assertThat(content, hasItems(
                Bottle.newInstance(GREEN, 23),
                Bottle.newInstance(GREEN, 23),
                Bottle.newInstance(BROWN, 23),
                Bottle.newInstance(BROWN, 23),
                Bottle.newInstance(BROWN, 23),
                Bottle.newInstance(WHITE, 65)));
    }

    @Test
    public void testMapIdentityLaw() {
        Crate<Bottle> crateAfterMap = crate.map(Function.identity());
        assertThat(crateAfterMap, is(crate));
    }

    @Test
    public void testMapAssociativeLaw() {
        Crate<Bottle> crate1 = crate.map(Bottle::heat).map(b -> b.drink(10));
        Crate<Bottle> crate2 = crate.map(b -> b.heat().drink(10));

        assertThat(crate1, is(crate2));
    }



    private static Crate doubleAndCrate(Bottle b) {
        return Crate.of(
                Bottle.newInstance(b.getColor(), b.getTemperature(), b.getAmountOfLiquidInCl()),
                Bottle.newInstance(b.getColor(), b.getTemperature(), b.getAmountOfLiquidInCl())
        );
    }

    private static Crate<Bottle> drinkAndCrate(Bottle b) {
        return Crate.of(b.drink(10));
    }

    @Test
    public void testFlatMap() {
        Crate<Bottle> crateAfterFlatMap = crate.flatMap(CrateTest::doubleAndCrate);

        assertThat(crateAfterFlatMap, isA(Crate.class));
        assertThat(crateAfterFlatMap.getContent(), hasItems(
                Bottle.newInstance(GREEN, 33),
                Bottle.newInstance(GREEN, 33),
                Bottle.newInstance(GREEN, 33),
                Bottle.newInstance(GREEN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(BROWN, 33),
                Bottle.newInstance(WHITE, 75),
                Bottle.newInstance(WHITE, 75)
                ));
    }
/*
    @Test
    public void testFlatMapLeftIdentityLaw() {
        Crate<Bottle> crateAfterFlatMao = crate.flatMap(CrateTest::doubleAndCrate)

        Crate<Bottle> directFlatMap = doubleAndCrate(bottles);

        assertThat(crateAfterFlatMao, is(directFlatMap));
    }

    @Test
    public void testFlatMapRightIdentityLaw() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfPeanutsAfterFlatMap = bagOfPeanuts.flatMap(Bag::of);

        assertThat(bagOfPeanutsAfterFlatMap, is(bagOfPeanuts));
    }

    @Test
    public void testFlatMapAssociativeLaw() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfPeanutsAfterFlatMap1 = bagOfPeanuts.flatMap(CrateTest::saltAndBag).flatMap(CrateTest::roastAndBag);
        Bag<Peanuts> bagOfPeanutsAfterFlatMap2 = bagOfPeanuts.flatMap(p -> saltAndBag(p).flatMap(CrateTest::roastAndBag));

        assertThat(bagOfPeanutsAfterFlatMap1, is(bagOfPeanutsAfterFlatMap2));
    }


    @Test
    public void testEmptyBag() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
    }

    @Test(expected = IllegalStateException.class)
    public void testEmptyBagGetContents() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Peanuts peanuts = emptyBagOfPeanuts.getContent();
    }



    @Test
    public void testIsEmptyBag() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        assertThat(bagOfPeanuts.isPresent(), is(true));

        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        assertThat(emptyBagOfPeanuts.isPresent(), is(false));
    }



    @Test
    public void testEmptyBagMap() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Bag<Peanuts> emptyBagOfRoastedPeanuts = emptyBagOfPeanuts.map(Peanuts::roast);
        assertThat(emptyBagOfRoastedPeanuts.isPresent(), is(false));
    }



    @Test
    public void testEmptyBagFlatMap() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Bag<Peanuts> emptyBagOfSaltedPeanuts = emptyBagOfPeanuts.flatMap(CrateTest::saltAndBag);
        assertThat(emptyBagOfSaltedPeanuts.isPresent(), is(false));
    }



    @Test
    public void testEmptyBagFlatten() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Bag<Bag<Peanuts>> bagOfemptyBagOfPeanuts = Bag.of(emptyBagOfPeanuts);
        Bag<Peanuts> flattenedBagOfPeanuts = flatten(bagOfemptyBagOfPeanuts);
        assertThat(flattenedBagOfPeanuts.isPresent(), is(false));
    }



    @Test
    public void testMapToNull() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfPeanutsAfterMap = bagOfPeanuts.map(p -> null);
        assertThat(bagOfPeanutsAfterMap.isPresent(), is(false));
    }



    @Test
    public void testFilterPresent() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfSaltedPeanuts = bagOfPeanuts.map(Peanuts::salt);

        Bag<Peanuts> bagOfSaltedPeanutsFilterSalted = bagOfSaltedPeanuts.filter(p -> p.isSalted());

        assertThat(bagOfSaltedPeanutsFilterSalted.isPresent(), is(true));

    }



    @Test
    public void testFilterNotPresent() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfSaltedPeanuts = bagOfPeanuts.map(Peanuts::salt);

        Bag<Peanuts> bagOfSaltedPeanutsFilterRoasted = bagOfSaltedPeanuts.filter(p -> p.isRoasted());

        assertThat(bagOfSaltedPeanutsFilterRoasted.isPresent(), is(false));
    }



    @Test
    public void testEmptyFilter() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Bag<Peanuts> bagOfSaltedPeanuts = emptyBagOfPeanuts.map(Peanuts::salt);

        Bag<Peanuts> bagOfSaltedPeanutsFilterSalted = bagOfSaltedPeanuts.filter(p -> p.isSalted());

        assertThat(bagOfSaltedPeanutsFilterSalted.isPresent(), is(false));
    }

*/

    protected enum Color {
        GREEN,
        BROWN,
        WHITE
    }

    private static class Bottle {
        private int amountOfLiquidInCl;
        private int temperature;
        private Color color;

        private Bottle(Color color, int temperature, int amountOfLiquidInCl) {
            this.color = color;
            this.temperature = temperature;
            this.amountOfLiquidInCl = amountOfLiquidInCl;
        }

        public static Bottle newInstance() {
            return new Bottle(GREEN, 20,33);
        }

        public static Bottle newInstance(Color color, int amount) {
            return new Bottle(color, 20, amount);
        }

        public static Bottle newInstance(Color color, int temperature, int amount) {
            return new Bottle(color, temperature, amount);
        }

        public Bottle drink(int amount) {
            return new Bottle(getColor(), getTemperature(), Math.max(0, getAmountOfLiquidInCl() - amount));
        }

        public Bottle heat() {
            return new Bottle(getColor(), getTemperature() + 5, getAmountOfLiquidInCl());
        }

        public Bottle cool() {
            return new Bottle(getColor(), getTemperature() - 5 , getAmountOfLiquidInCl());
        }


        public int getAmountOfLiquidInCl() {
            return amountOfLiquidInCl;
        }

        public int getTemperature() {
            return temperature;
        }

        public Color getColor() {
            return color;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Bottle bottle = (Bottle) o;
            return amountOfLiquidInCl == bottle.amountOfLiquidInCl &&
                    color == bottle.color;
        }

        @Override
        public int hashCode() {
            return Objects.hash(amountOfLiquidInCl, color);
        }

        public String toString() {
            return color.name()+" Bottle with "+amountOfLiquidInCl+" cl liquid.";
        }
    }
}
