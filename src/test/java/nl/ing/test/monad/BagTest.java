package nl.ing.test.monad;

import org.junit.Test;

import java.util.function.Function;

import static nl.ing.test.monad.Bag.flatten;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.core.IsNull.notNullValue;

public class BagTest {

    @Test
    public void testOf() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        assertThat(bagOfPeanuts, notNullValue());
        assertThat(bagOfPeanuts.getContent(), is(peanuts));
    }

    @Test(expected = NullPointerException.class)
    public void testOfNull() {
        Peanuts peanuts = null;
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
    }


    @Test
    public void testMap() {
        Peanuts peanuts = Peanuts.newInstance();
        assertThat(peanuts.isRoasted(), is(false));
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        assertThat(bagOfPeanuts.getContent().isRoasted(), is(false));
        Bag<Peanuts> bagOfRoastedPeanuts = bagOfPeanuts.map(p -> p.roast());

        assertThat(bagOfRoastedPeanuts.getContent().isRoasted(), is(true));
    }



    @Test
    public void testMapIdentityLaw() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfPeanutsAfterMap = bagOfPeanuts.map(Function.identity());
        assertThat(bagOfPeanutsAfterMap, is(bagOfPeanuts));
    }




    @Test
    public void testMapAssociativeLaw() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfRoastedSaltedPeanuts1 = bagOfPeanuts.map(Peanuts::roast).map(Peanuts::salt);
        Bag<Peanuts> bagOfRoastedSaltedPeanuts2 = bagOfPeanuts.map(p -> p.roast().salt());
        assertThat(bagOfRoastedSaltedPeanuts1, is(bagOfRoastedSaltedPeanuts2));
    }



    private static Bag<Peanuts> saltAndBag(Peanuts p) {
        return Bag.of(p.salt());
    }

    private static Bag<Peanuts> roastAndBag(Peanuts p) {
        return Bag.of(p.roast());
    }

    @Test
    public void testSaltAndBag() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Bag<Peanuts>> bagOfBagOfSaltedPeanuts = bagOfPeanuts.map(BagTest::saltAndBag);

        assertThat(bagOfBagOfSaltedPeanuts.getContent(), isA(Bag.class));
        assertThat(bagOfBagOfSaltedPeanuts.getContent().getContent(), isA(Peanuts.class));
        assertThat(bagOfBagOfSaltedPeanuts.getContent().getContent().isSalted(), is(true));
    }


    @Test
    public void testFlatten() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Bag<Peanuts>> bagOfBagOfPeanuts = Bag.of(bagOfPeanuts);
        assertThat(bagOfBagOfPeanuts.getContent(), isA(Bag.class));
        assertThat(bagOfBagOfPeanuts.getContent().getContent(), isA(Peanuts.class));

        Bag<Peanuts> flattenedBagOfPeanuts = flatten(bagOfBagOfPeanuts);
        assertThat(flattenedBagOfPeanuts.getContent(), is(peanuts));
    }



    @Test
    public void testFlatMap() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfPeanutsAfterFlatMap = bagOfPeanuts.flatMap(BagTest::saltAndBag);

        assertThat(bagOfPeanutsAfterFlatMap, isA(Bag.class));
        assertThat(bagOfPeanutsAfterFlatMap.getContent(), isA(Peanuts.class));
        assertThat(bagOfPeanutsAfterFlatMap.getContent().isSalted(), is(true));
    }



    @Test
    public void testFlatMapLeftIdentityLaw() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfPeanutsAfterFlatMap = bagOfPeanuts.flatMap(BagTest::saltAndBag);
        Bag<Peanuts> saltedAndBaggedPeanuts = saltAndBag(peanuts);

        assertThat(bagOfPeanutsAfterFlatMap, is(saltedAndBaggedPeanuts));
    }

    @Test
    public void testFlatMapRightIdentityLaw() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfPeanutsAfterFlatMap = bagOfPeanuts.flatMap(Bag::of);

        assertThat(bagOfPeanutsAfterFlatMap, is(bagOfPeanuts));
    }

    @Test
    public void testFlatMapAssociativeLaw() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfPeanutsAfterFlatMap1 = bagOfPeanuts.flatMap(BagTest::saltAndBag).flatMap(BagTest::roastAndBag);
        Bag<Peanuts> bagOfPeanutsAfterFlatMap2 = bagOfPeanuts.flatMap(p -> saltAndBag(p).flatMap(BagTest::roastAndBag));

        assertThat(bagOfPeanutsAfterFlatMap1, is(bagOfPeanutsAfterFlatMap2));
    }


    @Test
    public void testEmptyBag() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
    }

    @Test(expected = IllegalStateException.class)
    public void testEmptyBagGetContents() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Peanuts peanuts = emptyBagOfPeanuts.getContent();
    }



    @Test
    public void testIsEmptyBag() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        assertThat(bagOfPeanuts.isPresent(), is(true));

        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        assertThat(emptyBagOfPeanuts.isPresent(), is(false));
    }



    @Test
    public void testEmptyBagMap() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Bag<Peanuts> emptyBagOfRoastedPeanuts = emptyBagOfPeanuts.map(Peanuts::roast);
        assertThat(emptyBagOfRoastedPeanuts.isPresent(), is(false));
    }



    @Test
    public void testEmptyBagFlatMap() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Bag<Peanuts> emptyBagOfSaltedPeanuts = emptyBagOfPeanuts.flatMap(BagTest::saltAndBag);
        assertThat(emptyBagOfSaltedPeanuts.isPresent(), is(false));
    }



    @Test
    public void testEmptyBagFlatten() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Bag<Bag<Peanuts>> bagOfemptyBagOfPeanuts = Bag.of(emptyBagOfPeanuts);
        Bag<Peanuts> flattenedBagOfPeanuts = flatten(bagOfemptyBagOfPeanuts);
        assertThat(flattenedBagOfPeanuts.isPresent(), is(false));
    }



    @Test
    public void testMapToNull() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfPeanutsAfterMap = bagOfPeanuts.map(p -> null);
        assertThat(bagOfPeanutsAfterMap.isPresent(), is(false));
    }



    @Test
    public void testFilterPresent() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfSaltedPeanuts = bagOfPeanuts.map(Peanuts::salt);

        Bag<Peanuts> bagOfSaltedPeanutsFilterSalted = bagOfSaltedPeanuts.filter(p -> p.isSalted());

        assertThat(bagOfSaltedPeanutsFilterSalted.isPresent(), is(true));

    }



    @Test
    public void testFilterNotPresent() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> bagOfPeanuts = Bag.of(peanuts);
        Bag<Peanuts> bagOfSaltedPeanuts = bagOfPeanuts.map(Peanuts::salt);

        Bag<Peanuts> bagOfSaltedPeanutsFilterRoasted = bagOfSaltedPeanuts.filter(p -> p.isRoasted());

        assertThat(bagOfSaltedPeanutsFilterRoasted.isPresent(), is(false));
    }



    @Test
    public void testEmptyFilter() {
        Bag<Peanuts> emptyBagOfPeanuts = Bag.empty();
        Bag<Peanuts> bagOfSaltedPeanuts = emptyBagOfPeanuts.map(Peanuts::salt);

        Bag<Peanuts> bagOfSaltedPeanutsFilterSalted = bagOfSaltedPeanuts.filter(p -> p.isSalted());

        assertThat(bagOfSaltedPeanutsFilterSalted.isPresent(), is(false));
    }


/*
    @Test
    public void testIfPresent() {
        Peanuts peanuts = Peanuts.newInstance();
        Bag<Peanuts> emptyBagOfPeanuts = Bag.of(peanuts);

        emptyBagOfPeanuts.ifPresent(p -> present = true);

        Consumer<Peanuts> it= Mockito.mock(Peanuts.class);

        instance.conditionalRun(it);

        Mockito.verify(it,Mockito.only()).accept(Mockito.any());


        assertThat(bagOfSaltedPeanutsFilterSalted.isPresent(), is(false));
    }
*/



    private static class Peanuts {
        private boolean roasted;
        private boolean salted;

        public static Peanuts newInstance() {
            return new Peanuts(false, false);
        }

        private Peanuts(boolean roasted, boolean salted) {
            this.roasted = roasted;
            this.salted = salted;
        }

        public boolean isRoasted() {
            return roasted;
        }

        public boolean isSalted() {
            return salted;
        }

        public Peanuts roast() {
            return new Peanuts(true, isSalted());
        }

        public Peanuts salt() {
            return new Peanuts(isRoasted(), true);
        }

        @Override
        public boolean equals(Object other) {
            if (other == null) return false;
            if (!(other instanceof Peanuts)) return false;
            return this.isRoasted() == ((Peanuts)other).isRoasted() &&
                    this.isSalted() == ((Peanuts)other).isSalted();
        }
    }
}
