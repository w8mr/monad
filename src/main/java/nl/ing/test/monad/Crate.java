package nl.ing.test.monad;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

public class Crate<T> {
    private List<T> content;

    private Crate(List<T> content) {
        this.content = content;
    }

    public static <T> Crate<T> of(List<T> content) {
        if (content == null) throw new NullPointerException("Please put something in the crate.");
        return new Crate(content);
    }

    public static <T> Crate<T> of(T... content) {
        if (content == null) throw new NullPointerException("Please put something in the crate.");
        return new Crate(Arrays.asList(content));
    }

    public List<T> getContent() {
        return content;
    }

    public <R> Crate<R> map(Function<T, R> mapFunction) {
        return flatMap(c -> Crate.of(mapFunction.apply(c)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Crate<?> bag = (Crate<?>) o;
        return Objects.equals(content, bag.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content);
    }

    public static <T> Crate<T> flatten(Crate<Crate<T>> doubleCrate) {
        return doubleCrate.flatMap(b -> b);
    }

    public <R> Crate<R> flatMap(Function<T, Crate<R>> flatMapFunction) {
        List<R> result = new ArrayList(content.size());
        for (T t : content) {
            Crate<R> crate = flatMapFunction.apply(t);
            for (R r: crate.getContent()) {
                result.add(r);
            }
        }
        return Crate.of(result);
    }

    public Crate<T> filter(Predicate<T> predicate) {
        List<T> result = new ArrayList(content.size());
        for (T t : content) {
            if (predicate.test(t)) {
                result.add(t);
            }
        }
        return Crate.of(result);
    }
}
