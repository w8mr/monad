package nl.ing.test.monad;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Bag<T> {
    private T content;

    private Bag(T content) {
        this.content = content;
    }

    public static <T> Bag<T> of(T content) {
        if (content == null) throw new NullPointerException("Please put something in the bag.");
        return new Bag(content);
    }

    public T getContent() {
        if (content == null) throw new IllegalStateException();
        return content;
    }

    public boolean isPresent() {
        return content != null;
    }

    public <R> Bag<R> map(Function<T, R> mapFunction) {
        return flatMap(c -> Bag.ofNullable(mapFunction.apply(c)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bag<?> bag = (Bag<?>) o;
        return Objects.equals(content, bag.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content);
    }

    public static <T> Bag<T> flatten(Bag<Bag<T>> doubleBag) {
        return doubleBag.flatMap(b -> b);
    }

    public <R> Bag<R> flatMap(Function<T, Bag<R>> flatMapFunction) {
        return content == null ?
                Bag.empty() :
                flatMapFunction.apply(getContent());
    }

    public static <T> Bag<T> empty() {
        return new Bag(null);
    }

    public static <T> Bag<T> ofNullable(T content) {
        return content == null ?
                empty() :
                of(content);
    }

    public Bag<T> filter(Predicate<T> predicate) {
        return content == null ?
                empty() :
                (
                        predicate.test(getContent()) ?
                        Bag.of(getContent()) :
                        empty()
                );
    }
}
